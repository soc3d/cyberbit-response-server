var PythonShell= require('python-shell');

module.exports=
    {
        phishingScenario: function (wfID, rField, rValue, daysBack, res) {

            var options = {
                mode: 'text',
                pythonPath: 'C:/python 27_14/python.exe',
                pythonOptions: ['-u'], // get print results in real-time
                scriptPath: 'C:\\Tom\'s Code\\integrations\\SOC3D_API_V2\\',
                args: [wfID, rField, rValue, daysBack]
            };
            PythonShell.run('PhishingAutomation.py', options, function (err, results) {
                if (err) throw res.send(err);
                // results is an array consisting of messages collected during execution
                console.log('results: %j', results);
                res.send(results)
            });
        }
    }

var apiKey = "95d362bc20946172c059611c765f7620da76f98ab4a202565b66cc4bafea9ed9";
var vt = require("node-virustotal");

function createConnection() {
    var con = vt.MakePublicConnection();
    con.setKey(apiKey);
    console.log(con.getKey());
    return con;
}
module.exports = {
    getReportForHash: function (fileHash,res) {
        console.log('in hash func');
        var conn = createConnection();
        conn.getFileReport(fileHash, function (data) {
            console.log(data);
            res.status(200).send(data)
        }, function (mistake) {
            console.log(mistake);
            res.status(500).send("Unknown error")
        });
    }
}
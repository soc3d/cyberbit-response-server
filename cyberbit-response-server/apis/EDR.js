var readfiles = require('node-readfiles');

module.exports = {
    getLogFiles: function (res) {
        var logs=[]
        readfiles('C:\\jsons', function (err, filename, contents) {
            if (err) throw err;
            console.log('File ' + filename + ':');
            console.log(contents);
            logs.push(JSON.parse(contents))
        }).then(function (files) {
            console.log('Read ' + files.length + ' file(s)');
            res.send(logs)
        }).catch(function (err) {
            console.log('Error reading files:', err.message);
        });
    }
}


var request = require('request');
var fs=require('fs');
var opn= require('opn');

var visualizerURL="https://oasis-open.github.io/cti-stix-visualization/?url=https://raw.githubusercontent.com/oasis-open/cti-stix-visualization/master/"

function getJsonString(path) {
    var jsonString;
    jsonString=JSON.parse(fs.readFileSync(path,'utf8'));
    console.log(jsonString);
    return jsonString;
}
module.exports = {
    visualizeJson: function (url,res) {
        console.log('visualizing');
        opn(visualizerURL+url);
    }
}
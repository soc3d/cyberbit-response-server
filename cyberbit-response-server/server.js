// =======================
// get packages ==========
// =======================
var express = require('express');
var app =module.exports= express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var config = require('./config'); // get our config file
var virusTotalRoutes=require('./routes/virusTotal');
var apiRoutes=require('./routes/manage');
var User = require('./models/user'); // get our mongoose model
var basicRoutes=require('./routes/basicRoutes');
var xForceRoutes=require('./routes/xForceRoutes');
var STIXroutes=require('./routes/STIXroutes');
var responseRoutes=require('./routes/ResponseRoutes');
var path=require('path');
var EDRroutes=require('./routes/EDRRoutes')




// =======================
// configuration =========
// =======================

mongoose.connect(config.database, {useMongoClient: true}); // connect to database
app.set('superSecret', config.secret); // secret variable
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
// use morgan to log requests to the console
app.use(morgan('dev'));
app.use(express.static('Views'));




// =======================
// start the server ======
// =======================

app.use('/api', apiRoutes);
app.use('/api/virustotal',virusTotalRoutes);
app.use('/api/xforce',xForceRoutes);
app.use('/api/STIX',STIXroutes);
app.use('/api/response',responseRoutes);
app.use('/api/EDR',EDRroutes);
app.listen(config.serverPort);
console.log('Welcome to Cyberbit Response Server @ http://localhost:' + config.serverPort);

var app = require('./../server.js');
var User = require('./../models/user'); // get our mongoose model
var config=require('./../config');


// basic routes
app.get('/', function (req, res) {
    res.send('Hello! SOC3D Response server API is at http://localhost:' + config.serverPort + '/api');
});
app.get('/setup', function (req, res) {

    // create a sample user
    var tom = new User({
        name: 'cool ',
        password: 'mind',
        admin: true
    });

    // save the sample user
    tom.save(function (err) {
        if (err) throw err;
        console.log('User saved successfully');
        res.json({success: true});
    });
});

app.get('/login', function(req, res) {
    console.log(config.env);
    res.sendFile(config.env+'/Views/Login/Login.html');
});
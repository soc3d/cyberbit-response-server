var express = require('express');
var EDRRoutes = express.Router();
var EDRAPI = require('./../apis/EDR');
var Authentication = require('./../Authentication');
var app=require('./../server.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens



EDRRoutes.use(function(req, res, next) {
    Authentication.authenticate(app,jwt,req,res,next);
});
EDRRoutes.get('/', function (req, res) {
    res.json({message: 'EDR Management module.supported features: 1.GetLogFiles (/getLogFiles)'});
});

EDRRoutes.get('/getLogFiles',function(req,res){
    EDRAPI.getLogFiles(res);
    console.log("Getting EDR logs");
});
module.exports=EDRRoutes;




var express = require('express');
var xForceRoutes = express.Router();
var xForceApi = require('./../apis/xForce');
var Authentication = require('./../Authentication');
var app=require('./../server.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

xForceRoutes.use(function(req, res, next) {
    Authentication.authenticate(app,jwt,req,res,next);
});

xForceRoutes.get('/', function (req, res) {
    res.json({message: 'SOC3D xForce integration module.supported features: 1.Get report for URL (/getReportForURL)'});
});

xForceRoutes.get('/url/:url', function (req, res) {
    xForceApi.getReportForURL(req.params.url,res);
});

module.exports=xForceRoutes;
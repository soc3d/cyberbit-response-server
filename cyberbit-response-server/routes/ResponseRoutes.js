var express = require('express');
var responseRoutes = express.Router();
var responseAPI = require('./../Tools/Phishing');
var Authentication = require('./../Authentication');
var app=require('./../server.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

responseRoutes.use(function(req, res, next) {
    Authentication.authenticate(app,jwt,req,res,next);
});

responseRoutes.get('/', function (req, res) {
    res.json({message: 'SOC3D Tools integration module.supported features:'});
});

responseRoutes.post('/phishing', function (req, res) {
    responseAPI.phishingScenario(req.body.wfID,req.body.rField,req.body.rValue,req.body.daysBack,res);
});

module.exports=responseRoutes;
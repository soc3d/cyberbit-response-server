var express = require('express');
var STIXRoutes = express.Router();
var STIXApi = require('./../apis/STIX');
var Authentication = require('./../Authentication');
var app=require('./../server.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens



STIXRoutes.use(function(req, res, next) {
    Authentication.authenticate(app,jwt,req,res,next);
});
STIXRoutes.get('/', function (req, res) {
    res.json({message: 'SOC3D STIX integration module.supported features: 1.Visualize JSON (/visualize)'});
});

STIXRoutes.get('/visualize',function(req,res){
    STIXApi.visualizeJson(req.headers['url'],res);
    console.log("url: "+req.headers['url']);
});
module.exports=STIXRoutes;



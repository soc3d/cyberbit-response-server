var express = require('express');
var virusTotalRoutes = express.Router();
var virusTotalApi = require('./../apis/virusTotal');
var Authentication = require('./../Authentication');
var app=require('./../server.js');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens



virusTotalRoutes.use(function(req, res, next) {
    Authentication.authenticate(app,jwt,req,res,next);
});
virusTotalRoutes.get('/', function (req, res) {
    res.json({message: 'SOC3D virusTotal integration module.supported features: 1.Get report for hash (/getHashScan)'});
});

virusTotalRoutes.get('/getHashScan',function(req,res){
    virusTotalApi.getReportForHash(req.headers['hash'],res);
    console.log("Hash: "+req.headers['hash']);
});
module.exports=virusTotalRoutes;




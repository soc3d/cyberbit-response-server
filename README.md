# Cyberbit Response Server
Cyberbit Response and General operations platform .Currently supports SOC3D response platform and some EDR logging cpabilities
## Pre-Requisites
A mongoDB instance should be running. Edit the connection string under config.js
## Installation
Open a git bash and type:
```
$ git clone https://SOC3DPS@bitbucket.org/soc3d/cyberbit-response-server.git
```

go inside the server directory by typing : 

```
cd cyberbit-response-server
```

Now in order to install the required modules , run :
```
$ npm install
```
Now run the server:
```
$ node server.js
```

